Stéganographie avec des perles à repasser
==========================================
 
* représentation numérique d'une image par une grille de pixels
* représentation des teintes de pixels par des nombre
* découverte de la stéganographie ou comment dissimuler la présence d'information

Jeu de Nim
===========

Faire découvir à l'élève une stratégie, puis formaliser un algorithme gagnant, au jeu de Nim. 
Éventuellement programmer ceta algorithme avec Scratch.


Codage d'images et stéganographie
=======================

Découvrir la notion de codage pour des objets complexes (des images). Détailler une méthode de 
stéganographie, inspirée de la stéganographie LSB (Least Significant Bits).

Les haricots de Dijkstra
================

Analyser un jeu, découvir une stratégie (qui doit commencer ?) pour gagner au jeu des haricots de Dijkstra.


Cryptogaphie Symétrique
===================

Expliquer les principes de chiffrement symétrique à travers la
scytale et le chiffrement de César. 

Pixel art et algorithmique
=================

* Découvrir la notion d'algorithme avec un jeu d'instructions très limité pour réaliser des dessins 
* Exécuter un algorithme sans se tromper 
* Découvrir les briques de base des algorithmes et créer des algorithmes de dessin 
* Découvrir la notion de bug informatique 


Crypto Asymétrique
==============

Comprendre le concepts de chiffrement asymétrique (clé privé / clé publique) en résolvant une énigme.

Tri
======

* Comprendre quelques algorithmes de tris (tri 2 couleurs, 3 couleurs, selection, insertion).
* Identifier les critères d’arrêt d’un algorithme
* Appréhender les notions de complexité (au pire).
